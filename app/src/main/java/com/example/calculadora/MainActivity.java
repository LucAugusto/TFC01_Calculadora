package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int operation;
    private double v1;
    private double v2;
    private double aux = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickNumbers(View view){
        String numero = ((Button) view).getText().toString();
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        visor.setText(visor.getText()+numero);
        historico.append(numero);
    }

    public void onClickClear(View view){
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        this.aux = 0;
        visor.setText("");
        historico.setText("");


    }

    public void onClickDelete(View view){

        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        String txtVisor = visor.getText().toString();


        if(!txtVisor.isEmpty()) {
            txtVisor = txtVisor.substring(0, txtVisor.length() - 1);
        }
        visor.setText(txtVisor);

    }

    public void onClickPlus(View view){

        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        String vava = visor.getText().toString();
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        if(!vava.isEmpty()) {
            this.v1 = Double.parseDouble(vava);
            visor.setText("");
            if(this.aux != 0) {
                if (this.operation == 2) {
                    this.aux = this.aux - this.v1;
                } else {
                    if (this.operation == 3) {
                        this.aux = this.aux * this.v1;
                    }
                    this.aux = this.aux + this.v1;

                }
            }else{
                this.aux = this.v1;
            }
        }
    historico.append(" + ");
    this.operation = 1;
    }

    public void onClickMinus(View view){
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        String vava = visor.getText().toString();



        if(!vava.isEmpty()) {
            this.v1 = Double.parseDouble(vava);
            visor.setText("");
            if(this.aux != 0) {
                if(this.operation == 1){
                    this.aux = this.aux + this.v1;
                } else {
                    if(this.operation == 3){
                        this.aux = this.aux * this.v1;
                    }
                        this.aux = this.aux - this.v1;

                }
            }else{
                    this.aux = this.v1; //SE IGUAL A ZERO
            }
        }else{
            visor.setText("-");
        }

        historico.append(" - ");
        this.operation = 2;
    }

    public void onClickMultiply(View view){
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        String vava = visor.getText().toString();

        if(!vava.isEmpty()) {
            this.v1 = Double.parseDouble(vava);
            visor.setText("");
            if(this.aux != 0) {
                if (this.operation == 2) {
                    this.aux = this.aux - this.v1;
                } else {
                    if (this.operation == 1) {
                        this.aux = this.aux + this.v1;
                    }

                }
                this.aux = this.aux * this.v1;
            }else{
                this.aux = this.v1;
            }

        }

    historico.append(" * ");
    this.operation = 3;
    }

    public void onClickDivide(View view){
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        String vava = visor.getText().toString();

        if(!vava.isEmpty()) {
            this.v1 = Double.parseDouble(vava);
            visor.setText("");
            if(this.aux != 0) {
                if (this.operation == 2) {
                    this.aux = this.aux - this.v1;
                } else {
                    if (this.operation == 1) {
                        this.aux = this.aux + this.v1;
                    }
                    if(this.operation == 3){
                        this.aux = this.aux * this.v1;
                    }

                }
                this.aux = this.aux / this.v1;
            }else{
                this.aux = this.v1;
            }

        }

        historico.append(" / ");
        this.operation = 4;
    }

    public void onClickEquals(View view) {
        TextView visor = ((TextView) findViewById(R.id.txt_visor));
        TextView historico = ((TextView) findViewById(R.id.txt_historico));
        String vava = visor.getText().toString();
        if (!vava.isEmpty()) {
            this.v2 = Double.parseDouble(visor.getText().toString());
            if (this.operation == 1) {
                double soma = (this.aux + this.v2);
                visor.setText("" + soma);
                historico.setText(""+soma);
                this.aux = 0;
            }
            if(this.operation == 2){
                double subtracao = (this.aux - this.v2);
                visor.setText(""+subtracao);
                historico.setText(""+subtracao);
                this.aux = 0;
            }
            if(this.operation == 3){
                double multiplicacao = (this.aux * this.v2);
                visor.setText(""+multiplicacao);
                historico.setText(""+multiplicacao);
                this.aux = 0;
            }
            if(this.operation == 4){
                double divisao = (this.aux / this.v2);
                visor.setText(""+divisao);
                historico.setText(""+divisao);
                this.aux = 0;
            }

        }

    }
}